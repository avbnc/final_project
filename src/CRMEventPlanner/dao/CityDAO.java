package CRMEventPlanner.dao;
import java.sql.*;
import java.util.ArrayList;
//import CRMEventPlanner.bean.EventBean;

public class CityDAO {
	private Connection con = null;
    private PreparedStatement stmt;
    private ResultSet rs = null;
    
    public ArrayList<CityMap> SelectCity() throws SQLException {
       CRMConnection CRMcon = CRMConnection.getInstance();
 	   String sql = "SELECT city, id FROM salesforce.luEventCities order by city asc";
 	   try {     
 			con = CRMcon.getConnection();
 			stmt = con.prepareStatement(sql);
 			rs = stmt.executeQuery();
 			
 			ArrayList<CityMap> CityList = new ArrayList<CityMap>();        
 			while (rs.next()){
 				CityList.add(new CityMap(rs.getString("city"), Integer.parseInt(rs.getString("id"))));
 			}
 			return CityList;
 		}
 		catch (SQLException sqe){
 			throw new SQLException(sqe);
 		}
 		finally {
 			CRMcon.closeResultSet(rs);
 			CRMcon.closeStatement(stmt);
 			CRMcon.closeConnection(con);
 		}
   }
}
