package CRMEventPlanner.bean;

import java.util.Date;

public class EventBean implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1317756785342854730L;
	private String eventName;
	private String eventCategory;
	private String eventPlace;
	private String eventAddress;
	private String eventCompany;
	private String eventCity;
	private String eventLOB;
	private String eventOrganizer;
	private Integer eventCapacity;
	private String eventStartDate;	
	private String eventEntDate;
	private Integer eventReservation;
	private String eventComments;
	
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventCompany() {
		return eventCompany;
	}
	public void setEventCompany(String eventCompany) {
		this.eventCompany = eventCompany;
	}
	
	public String getEventCity() {
		return eventCity;
	}
	public void setEventCity(String eventCity) {
		this.eventCity = eventCity;
	}
	
	public String getEventLOB() {
		return eventLOB;
	}
	public void setEventLOB(String eventLOB) {
		this.eventLOB = eventLOB;
	}
	
	public String getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}
	
	public String getEventPlace() {
		return eventPlace;
	}
	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}
	public String getEventAddress() {
		return eventAddress;
	}
	public void setEventAddress(String eventAddress) {
		this.eventAddress = eventAddress;
	}
	
	public String getEventOrganizer() {
		return eventOrganizer;
	}
	public void setEventOrganizer(String eventOrganizer) {
		this.eventOrganizer = eventOrganizer;
	}
	
	public Integer getEventCapacity() {
		return eventCapacity;
	}
	public void setEventCapacity(Integer eventCapacity) {
		this.eventCapacity = eventCapacity;
	}
	public String getEventStartDate() {
	return eventStartDate;
	}
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	public String getEventEndDate() {
	return eventEntDate;
	}
	public void setEventEndDate(String eventEntDate) {
		this.eventEntDate = eventEntDate;
	}

	public Integer getEventReservation() {
	return eventReservation;
	}
	public void setEventReservation(Integer eventReservation) {
		this.eventReservation = eventReservation;
	}

	public String getEventComments() {
	return eventComments;
	}
	public void setEventComments(String eventComments) {
		this.eventComments = eventComments;
	}
	
}
